package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		int onCreateCounter = 0;
		int onStartCounter = 0;
		int onResumeCounter = 0;
		int onPauseCounter = 0;
		int onStopCounter = 0;
		int onDestroyCounter = 0;
		int onRestartCounter = 0;
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.
		TextView create;
		TextView start;
		TextView resume;
		TextView pause;
		TextView stop;
		TextView destroy;
		TextView restart;
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			onCreateCounter++;
			setContentView(R.layout.activity_one);
            create = (TextView) findViewById(R.id.create);
            start = (TextView) findViewById(R.id.start);
            resume = (TextView) findViewById(R.id.resume);
            pause = (TextView) findViewById(R.id.pause);
            stop = (TextView) findViewById(R.id.stop);
            destroy = (TextView) findViewById(R.id.destroy);
            restart = (TextView) findViewById(R.id.restart);
            //Log cat print out
			if (savedInstanceState != null){
				if(savedInstanceState.getInt("onCreateCounter")!= 0){
					onCreateCounter = savedInstanceState.getInt("onCreateCounter");
				}
				if(savedInstanceState.getInt("onStartCounter")!= 0){
					onStartCounter = savedInstanceState.getInt("onStartCounter");
				}
				if(savedInstanceState.getInt("onResumeCounter")!= 0){
					onResumeCounter = savedInstanceState.getInt("onResumeCounter");
				}
				if(savedInstanceState.getInt("onPauseCounter") != 0){
					onPauseCounter = savedInstanceState.getInt("onPauseCounter");
				}
				if(savedInstanceState.getInt("onStopCounter") != 0){
					onStopCounter = savedInstanceState.getInt("onStopCounter");
				}
				if(savedInstanceState.getInt("onDestroyCounter") != 0){
					onDestroyCounter = savedInstanceState.getInt("onDestroyCounter");
				}
				if(savedInstanceState.getInt("onRestartCounter") != 0){
					onRestartCounter = savedInstanceState.getInt("onRestartCounter");
				}
			}
            updateLabels();
			Log.i(TAG, "onCreate called");
			//TODO: update the appropriate count variable & update the view
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides

        public void updateLabels() {
            create.setText("onCreate called:" + onCreateCounter);
            start.setText("OnStart called: " + onStartCounter);
            resume.setText("OnResume called: " + onResumeCounter);
            pause.setText("OnPause called: " + onPauseCounter);
            stop.setText("OnStop called: " + onStopCounter);
            destroy.setText("OnDestroy called: " + onDestroyCounter);
            restart.setText("OnRestart called: " + onRestartCounter);
        }

		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart called");
			onStartCounter++;
            updateLabels();

			//TODO:  update the appropriate count variable & update the view
		}
		@Override
		public void onResume(){
		super.onResume();

		onResumeCounter++;
		Log.i(TAG, "onResume called");
		updateLabels();

		//TODO:  update the appropriate count variable & update the view
	    }
	@Override
		public void onPause(){
		super.onPause();

		//Log cat print out
		onPauseCounter++;
		Log.i(TAG, "onPause called");
        updateLabels();
		//TODO:  update the appropriate count variable & update the view
	    }
	@Override
	public void onStop(){
		super.onStop();

		//Log cat print out
		Log.i(TAG, "onStop called");
		onStopCounter++;
        updateLabels();
		//TODO:  update the appropriate count variable & update the view
	    }
	@Override
	public void onDestroy(){
		super.onDestroy();

		//Log cat print out
		onDestroyCounter++;
        updateLabels();
		Log.i(TAG, "onDestroy called");
		//TODO:  update the appropriate count variable & update the view
	}
	@Override
	public void onRestart(){
		super.onRestart();

		//Log cat print out

        Log.i(TAG, "onRestart called");
		onRestartCounter++;
        updateLabels();
		//TODO:  update the appropriate count variable & update the view
	}


	    // TODO: implement 5 missing lifecycle callback methods

	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			savedInstanceState.putInt("onCreateCounter", onCreateCounter);
			savedInstanceState.putInt("onStartCounter", onStartCounter);
			savedInstanceState.putInt("onResumeCounter", onResumeCounter);
			savedInstanceState.putInt("onPauseCounter", onPauseCounter);
			savedInstanceState.putInt("onStopCounter", onStopCounter);
			savedInstanceState.putInt("onDestroyCounter", onDestroyCounter);
			savedInstanceState.putInt("onRestartCounter", onRestartCounter);
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
		}

		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
